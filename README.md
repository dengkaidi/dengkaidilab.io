# [日记](https://dengkaidi.github.io)

[![Like Google](https://github.com/dengkaidi/dengkaidi.github.io/static/img/Google.svg)][Google]
[![Linux Build Status](https://img.shields.io/travis/jekyll/jekyll/master.svg?label=Linux%20build)][travis]
[![Windows Build status](https://img.shields.io/appveyor/ci/jekyll/jekyll/master.svg?label=Windows%20build)][appveyor]
[![Test Coverage](https://img.shields.io/codeclimate/coverage/github/jekyll/jekyll.svg)][coverage]
[![Code Climate](https://img.shields.io/codeclimate/github/jekyll/jekyll.svg)][codeclimate]
[![Dependency Status](https://img.shields.io/gemnasium/jekyll/jekyll.svg)][gemnasium]
[![Security](https://hakiri.io/github/jekyll/jekyll/master.svg)][hakiri]

[Google]: https://www.google.com.hk 
[gemnasium]: https://gemnasium.com/jekyll/jekyll
[codeclimate]: https://codeclimate.com/github/jekyll/jekyll
[coverage]: https://codeclimate.com/github/jekyll/jekyll/coverage
[hakiri]: https://hakiri.io/github/jekyll/jekyll/master
[travis]: https://travis-ci.org/jekyll/jekyll
[appveyor]: https://ci.appveyor.com/project/jekyll/jekyll/branch/master

记录在学习过程中遇到所有觉得可以记录的东西
Java、Git、Debian、Shell、功能实现...
